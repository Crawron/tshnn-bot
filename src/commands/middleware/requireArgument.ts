import { Middleware } from "@enitoni/gears-discordjs"
import { UsageError } from "../types/UsageError"
import { CommandMetadata } from "../types/CommandMetadata"

export const requireArgument: (regex?: RegExp) => Middleware = (
  regex = /.+/g,
) => (context, next) => {
  const { content, issuer } = context
  const meta = issuer.metadata as CommandMetadata

  const argCount = (meta.arguments && meta.arguments.length) || 0

  if (!content.match(regex)) {
    throw new UsageError(
      issuer,
      `${meta.name} requires ${
        argCount === 1 ? "an argument" : `${argCount} arguments`
      }.`,
    )
  }

  return next()
}
