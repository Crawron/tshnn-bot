import { RichEmbed } from "discord.js"

export abstract class UserError extends Error {
  constructor(message?: string) {
    super(message)
  }

  abstract getEmbed(): RichEmbed
}
