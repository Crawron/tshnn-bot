import { Role, RichEmbed } from "discord.js"
import { Context } from "@enitoni/gears-discordjs"

import { UserError } from "./UserError"

export class RoleError extends UserError {
  constructor(
    public context: Context,
    public requiredRoles: Role[],
    message?: string,
  ) {
    super(message)
  }

  getEmbed() {
    const { requiredRoles } = this

    const embed = new RichEmbed({
      title: "Missing the following roles",
      description: requiredRoles.map((role) => role.name).join(", "),
    })

    return embed
  }
}
