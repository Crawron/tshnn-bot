import { Middleware } from "@enitoni/gears-discordjs"

import { RoleError } from "../types/RoleError"
import { ConfigService } from "../../services/storage/ConfigService"
import { checkMemberRoles } from "../helpers/checkMemberRoles"
import { getGuildRoles } from "../helpers/getGuildRoles"

export const requireGuildAdmin: () => Middleware = () => async (
  context,
  next,
) => {
  const { manager, message } = context
  const { member, guild } = message

  const configServ = manager.getService(ConfigService)
  const { adminRoles } = configServ.guildConfig

  const hasAdminRole = checkMemberRoles(member, ...adminRoles)
  const isAdmin = member.permissions.has("ADMINISTRATOR")

  const roleInstances = getGuildRoles(guild, adminRoles)
  if (!(hasAdminRole || isAdmin)) throw new RoleError(context, roleInstances)

  return next()
}
