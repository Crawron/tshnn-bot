import { Guild, Role } from "discord.js"

export function getGuildRoles(guild: Guild, roleIds: string[]): Role[] {
  return guild.roles
    .filter((guildRole) => roleIds.findIndex((id) => id === guildRole.id) >= 0)
    .array()
}
