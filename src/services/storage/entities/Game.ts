import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from "typeorm"
import { Member } from "./Member"
import { Poll } from "./Poll"

@Entity()
export class Game {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  name!: string

  @Column({ nullable: true })
  description?: string

  @Column({ nullable: true })
  link?: string

  @Column({ default: "pending" })
  state!: "pending" | "approved" | "rejected"

  @Column({ default: true })
  free!: boolean

  @ManyToMany(() => Poll)
  polls!: Poll[]

  @ManyToMany(() => Member)
  owners!: Member[]
}
