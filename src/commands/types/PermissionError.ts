import { PermissionResolvable, Permissions, RichEmbed } from "discord.js"
import { Context } from "@enitoni/gears-discordjs"

import { UserError } from "./UserError"
import { humanizePermissions } from "../helpers/humanizePermissions"

export class PermissionError extends UserError {
  constructor(
    public context: Context,
    public requiredPermissions: PermissionResolvable,
    message?: string,
  ) {
    super(message)
  }

  getEmbed() {
    const { requiredPermissions: permissions, context } = this
    const { member } = context.message

    const missingPerms = member && member.missingPermissions(permissions)

    const humanizedPerms = humanizePermissions(new Permissions(missingPerms))
    const embed = new RichEmbed({
      title: "Missing the following permissions",
      description: humanizedPerms,
    })

    return embed
  }
}
