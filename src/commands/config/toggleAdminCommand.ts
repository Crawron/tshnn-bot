import { matchPrefixes } from "@enitoni/gears"
import { CommandBuilder } from "@enitoni/gears-discordjs"

import { CommandMetadata } from "../types/CommandMetadata"
import { replyEmbed } from "../middleware/replyEmbed"
import { requireArgument } from "../middleware/requireArgument"
import { parseRoleArgument } from "../helpers/parseRoleArguments"
import { ConfigService } from "../../services/storage/ConfigService"

export const toggleAdminCommand = new CommandBuilder()
  .match(matchPrefixes("toggleAdmin", "ta", "admin"))
  .use(requireArgument())
  .use(replyEmbed())
  .use(async (context) => {
    const { state, manager } = context
    const { reply } = state

    const role = parseRoleArgument(context)
    if (!role) return reply("Couldn't find this role.")

    const configServ = manager.getService(ConfigService)

    if (configServ.checkAdmin(role.id)) {
      configServ.removeAdmin(role.id)
      return reply(`Removed \`${role.name}\` as bot admin role.`)
    } else {
      configServ.addAdmin(role.id)
      return reply(`Added \`${role.name}\` as bot admin role.`)
    }
  })
  .setMetadata<CommandMetadata>({
    name: "toggleAdmin",
    description: "Enables/Disables a role as a bot admin.",
    arguments: [
      {
        name: "Role",
        type: "ID, Role Name or Role Mention",
      },
    ],
    examples: [
      "toggleAdmin 581813610750410752",
      "ta @GamerRole",
      "admin PeopleRole",
    ],
  })
  .done()
