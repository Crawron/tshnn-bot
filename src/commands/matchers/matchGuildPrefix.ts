import { Matcher } from "@enitoni/gears-discordjs"
import { ConfigService } from "../../services/storage/ConfigService"

export const matchGuildPrefix: () => Matcher = () => async (context) => {
  const { manager, content } = context
  const configServ = manager.getService(ConfigService)

  const { prefix } = configServ.guildConfig
  const prefixRegex = new RegExp(`^${prefix}`)

  const hasPrefix = prefixRegex.test(content)
  if (!hasPrefix) return

  const newContent = content.replace(prefixRegex, "").trim()
  return { ...context, content: newContent }
}
