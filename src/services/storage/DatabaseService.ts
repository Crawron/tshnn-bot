import { Connection, createConnection } from "typeorm"
import { Service } from "@enitoni/gears-discordjs"

import { Game } from "./entities/Game"

import "reflect-metadata"

export class DatabaseService extends Service {
  private db!: Connection

  protected async serviceDidInitialize() {
    this.db = await createConnection()
  }

  async saveGame(game: Partial<Game>) {
    const { manager } = this.db

    const gameEnt = manager.create("Game", game)
    return manager.save(gameEnt)
  }

  async getAllGames(): Promise<Game[]> {
    return this.db.manager.find("Game")
  }
}
