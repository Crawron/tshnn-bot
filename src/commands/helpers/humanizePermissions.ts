import { Permissions } from "discord.js"

export function humanizePermissions(permissions: Permissions) {
	const str = permissions.toArray().join(", ")

	return str
		.toLowerCase()
		.replace(/_/g, " ")
		.replace(/\b\w/g, (sub) => sub.toUpperCase())
}
