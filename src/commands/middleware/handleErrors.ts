import { Middleware } from "@enitoni/gears-discordjs"

import { UserError } from "../types/UserError"
import { RichEmbed } from "discord.js"

export const handleErrors: () => Middleware = () => async (context, next) => {
  try {
    await next()
  } catch (err) {
    const { message } = context

    if (err instanceof UserError) {
      return message.channel.send(err.getEmbed())
    } else {
      console.log(err)
    }
  }
}
