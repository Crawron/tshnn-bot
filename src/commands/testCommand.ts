import { matchPrefixes } from "@enitoni/gears"
import { CommandBuilder } from "@enitoni/gears-discordjs"
import { replyEmbed } from "./middleware/replyEmbed"

export const testCommand = new CommandBuilder()
  .match(matchPrefixes("test"))
  .use(replyEmbed())
  .use(async (context) => {
    const { reply } = context.state
    reply("hello hi this test")
  })
  .done()
