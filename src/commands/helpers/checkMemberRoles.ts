import { GuildMember } from "discord.js"

export function checkMemberRoles(member: GuildMember, ...roleIds: string[]) {
  for (const requiredId of roleIds) {
    if (member.roles.has(requiredId)) return true
  }

  return false
}
