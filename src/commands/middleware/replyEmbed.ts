import { RichEmbed, Message } from "discord.js"
import { Middleware } from "@enitoni/gears-discordjs"

import { getBotColor } from "../helpers/getBotColor"

type ReplyEmbedState = {
  reply: (content: string) => Promise<Message | Message[]>
}

export const replyEmbed: () => Middleware<ReplyEmbedState> = () => async (
  context,
  next,
) => {
  const { message } = context

  const embed = new RichEmbed()

  const color = getBotColor(context)
  if (color) embed.setColor(color)

  context.state.reply = async (content) => {
    embed.setDescription(content)
    return message.channel.send(embed)
  }

  return next()
}
