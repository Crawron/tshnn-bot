import { Command, CommandGroup } from "@enitoni/gears-discordjs"
import { UserError } from "./UserError"
import { RichEmbed } from "discord.js"

export class UsageError extends UserError {
  constructor(public issuer: Command | CommandGroup, message?: string) {
    super(message)
  }

  getEmbed() {
    const { issuer, message } = this

    const embed = new RichEmbed({
      description: message,
    })

    return embed
  }
}
