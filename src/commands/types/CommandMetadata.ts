export type CommandMetadata = {
  name: string
  description: string
  arguments?: { name: string; type: string }[]
  examples?: string[]
}
