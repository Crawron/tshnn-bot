import { promisify } from "util"
import { readFile, writeFile } from "fs"
import { Service } from "@enitoni/gears-discordjs"

const writeFileAsync = promisify(writeFile)
const readFileAsync = promisify(readFile)

export class ConfigService extends Service {
  private config!: GuildConfig
  private configPath = process.env.CONFIG! || "config.json"

  async serviceDidInitialize() {
    try {
      this.config = await this.getStoredConfig()
    } catch (e) {
      this.config = {
        prefix: ",",
        adminRoles: [],
      }

      this.writeConfig()
    }
  }

  get guildConfig() {
    return this.config
  }

  setConfig(config: Partial<GuildConfig>) {
    this.config = { ...this.config, ...config }
    this.writeConfig()
  }

  addAdmin(roleId: string) {
    const { adminRoles } = this.config

    this.setConfig({
      adminRoles: [...adminRoles, roleId],
    })
  }

  removeAdmin(roleId: string) {
    const { adminRoles } = this.config

    this.setConfig({
      adminRoles: adminRoles.filter((role) => roleId !== role),
    })
  }

  checkAdmin(roleId: string) {
    return this.config.adminRoles.includes(roleId)
  }

  private async getStoredConfig(): Promise<GuildConfig> {
    const buffer = await readFileAsync(this.configPath)
    return JSON.parse(buffer.toString())
  }

  private async writeConfig() {
    writeFileAsync(this.configPath, JSON.stringify(this.config, undefined, 2))
  }
}

type GuildConfig = {
  prefix: string
  adminRoles: string[]
}
