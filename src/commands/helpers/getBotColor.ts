import { Context } from "@enitoni/gears-discordjs"

export function getBotColor(context: Context) {
  const { bot, message } = context
  const member = message.guild.member(bot.client.user)

  // member can be undefined. thanks, djs typings
  return member ? member.displayColor : 0
}
