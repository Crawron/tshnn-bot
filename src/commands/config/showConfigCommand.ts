import { matchPrefixes } from "@enitoni/gears"
import { CommandBuilder } from "@enitoni/gears-discordjs"

import { CommandMetadata } from "../types/CommandMetadata"
import { RichEmbed } from "discord.js"
import { getBotColor } from "../helpers/getBotColor"
import { ConfigService } from "../../services/storage/ConfigService"

export const showConfigCommand = new CommandBuilder()
  .match(matchPrefixes("config", "cfg", "showConfig"))
  .use(async (context) => {
    const { message, manager } = context
    const { guildConfig } = manager.getService(ConfigService)

    const botColor = getBotColor(context)

    const embed = new RichEmbed({
      title: `${message.guild.name}'s settings`,
    })

    const adminRoles = message.guild.roles
      .filter((guildRole) => guildConfig.adminRoles.includes(guildRole.id))
      .map((role) => role.name)
      .join(", ")

    embed.addField("Prefix", `\`${guildConfig.prefix}\``, true)
    embed.addField("Admin Roles", adminRoles || "*none*", true)
    if (botColor) embed.setColor(botColor)

    return message.channel.send(embed)
  })
  .setMetadata<CommandMetadata>({
    name: "showConfig",
    description:
      "Shows current guild settings: Bot prefix and bot admin roles.",
  })
  .done()
