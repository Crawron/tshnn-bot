import { Bot } from "@enitoni/gears"
import { Adapter } from "@enitoni/gears-discordjs"

import { DatabaseService } from "./services/storage/DatabaseService"
import { ConfigService } from "./services/storage/ConfigService"
import { prefixGroup } from "./commands/prefixGroup"

import "dotenv/config"

const adapter = new Adapter({ token: process.env.TOKEN! })

const bot = new Bot({
  adapter,
  commands: [prefixGroup],
  services: [ConfigService, DatabaseService],
})
bot.start()
