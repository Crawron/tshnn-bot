import { matchAlways } from "@enitoni/gears"
import { CommandGroupBuilder } from "@enitoni/gears-discordjs"

import { requireGuildAdmin } from "../middleware/requireGuildAdmin"
import { toggleAdminCommand } from "./toggleAdminCommand"
import { showConfigCommand } from "./showConfigCommand"

export const configGroup = new CommandGroupBuilder()
  .match(matchAlways())
  .use(requireGuildAdmin())
  .setCommands(toggleAdminCommand, showConfigCommand)
  .done()
