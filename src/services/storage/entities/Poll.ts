import {
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  OneToMany,
  JoinTable,
  Entity,
} from "typeorm"

import { Game } from "./Game"
import { Vote } from "./Vote"

@Entity()
export class Poll {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  running!: boolean

  @ManyToMany(() => Game)
  @JoinTable()
  games!: Game[]

  @OneToMany(() => Vote, (vote) => vote.poll)
  votes!: Vote[]
}
