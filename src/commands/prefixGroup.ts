import { CommandGroupBuilder } from "@enitoni/gears-discordjs"

import { matchGuildPrefix } from "./matchers/matchGuildPrefix"
import { handleErrors } from "./middleware/handleErrors"
import { testCommand } from "./testCommand"
import { configGroup } from "./config/configGroup"

export const prefixGroup = new CommandGroupBuilder()
  .match(matchGuildPrefix())
  .use(handleErrors())
  .setCommands(testCommand, configGroup)
  .done()
