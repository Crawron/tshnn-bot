import { Entity, PrimaryGeneratedColumn, ManyToOne } from "typeorm"

import { Poll } from "./Poll"
import { Member } from "./Member"

@Entity()
export class Vote {
  @PrimaryGeneratedColumn()
  id!: number

  @ManyToOne(() => Poll, (poll) => poll.votes)
  poll!: Poll

  @ManyToOne(() => Member, (member) => member.votes)
  member!: Member
}
