import { Role } from "discord.js"
import { Context } from "@enitoni/gears-discordjs"

export function parseRoleArgument({ content, message }: Context): Role {
  const mentions = message.mentions.roles.array().pop()
  if (mentions) return mentions

  const { guild } = message
  if (!guild) throw new Error("Can't use out of a guild")

  const namedRole = guild.roles.find((role, key) => {
    return role.name.includes(content) || key === content
  })

  return namedRole
}
