import {
  Entity,
  PrimaryColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from "typeorm"

import { Vote } from "./Vote"
import { Game } from "./Game"

@Entity()
export class Member {
  @PrimaryColumn()
  id!: string

  @OneToMany(() => Vote, (vote) => vote.member)
  votes!: Vote

  @ManyToMany(() => Game)
  @JoinTable()
  ownedGames!: Game[]
}
